const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/todo';

const client = new pg.Client(connectionString);
client.connect();
const query = client.query('CREATE TABLE todo (id SERIAL PRIMARY KEY, done BOOLEAN,text VARCHAR(144) not null');
query.on('end', () => { client.end(); });