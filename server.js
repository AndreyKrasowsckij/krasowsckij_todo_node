var express = require("express");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");

var app = express();
var jsonParser = bodyParser.json();
var sequelize = new Sequelize("postgres://dunice:ggs75bev11ds@localhost:5432/dunice");


app.use(express.static(__dirname + "/public"));


// Роутинги прям в файле сервера
//Добавление элемента в бд и возвращение всех значений
app.post("/insert", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    sequelize.query(`INSERT INTO todo (done_flag, text) VALUES (${request.body.done_flag}, '${request.body.text_value}')`)
    .then(() => {sequelize.query(`SELECT * FROM todo`).then(val => {response.json(val[0])})})
});

//Обновление статуса одной задачи
app.post("/update_private", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    sequelize.query(`UPDATE todo SET done_flag=${request.body.done_flag} WHERE id=${request.body.id_todo}`)
        .then(() => {sequelize.query(`SELECT * FROM todo`).then(val => {response.json(val[0])})})
});

//Обновлеие статуса всех задач
app.post("/update_public", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    sequelize.query(`UPDATE todo SET done_flag=${request.body.done_flag}`)
        .then(() => {sequelize.query(`SELECT * FROM todo`).then(val => {response.json(val[0])})})
});

//Удаление единственной задачи
app.post("/delete_private", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    sequelize.query(`DELETE FROM todo WHERE id=${request.body.id_todo}`)
        .then(() => {sequelize.query(`SELECT * FROM todo`).then(val => {response.json(val[0])})})
});

//Удаление всех выполненых задач
app.post("/delete_public", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    sequelize.query(`DELETE FROM todo WHERE done_flag=${request.body.done_flag}`)
        .then(() => {sequelize.query(`SELECT * FROM todo`).then(val => {response.json(val[0])})})
});

//Обновление текста
app.post("/update_text", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    sequelize.query(`UPDATE todo SET text='${request.body.text}' WHERE id=${request.body.id_todo}`)
        .then(() => {sequelize.query(`SELECT * FROM todo`).then(val => {response.json(val[0])})})
});

//Взятие значение текста записси
app.post("/select_text", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    sequelize.query(`SELECT text FROM todo WHERE id=${request.body.id_todo}`).then(val => {response.json(val[0])});
});

//Взятие всех записей для начала
app.post("/select_all", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    sequelize.query(`SELECT * FROM todo`).then(val => {response.json(val[0])});
});


app.listen(8002);

