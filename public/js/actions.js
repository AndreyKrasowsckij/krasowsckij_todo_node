var i;
var x;
var counter = 1;
var li_list = [];
var filter_status = 0;

// Снятие фокуса со всех редактируемых задач
function close_li() {
    for (i = 0; i < counter; i++) {
        state = $('.ul .li' + (i + 1));
        if (state.length > 0)
        {
            state.children('.li__div').addClass("light-mask");
            state.children('.li__div').removeClass("shadow-mask");
            state.children('.li__input-text').addClass("shadow-mask");
            state.children('.li__input-text').removeClass("light-mask");
            state.children('.li__div').children('.li__div__text').text(state.children('.li__input-text').val());
        }
    }
}

// Определение количества выполненых, невыполненных и всех задач
function count_all() {
    var result_all = 0;
    var result_done = 0;
    var result_not_done = 0;
    for (i = 0; i < counter; i++) {
        state = $('.ul .li' + (i + 1));
        if (state.length > 0)
        {
            result_all ++;
            if (state.children('.li__button').text() == "Выполнил")
            {
                result_done ++;
            }
            else
            {
                result_not_done ++;
            }
        }
    }
    result = {"res1": result_all, "res2": result_done, "res3": result_not_done};
    return result;
}

// Изменение строки состояния подсчёта
function recount() {
    var result_all = count_all().res1;
    var result_done = count_all().res2;
    var result_not_done = count_all().res3;
    $('.counter__all__num').html(result_all);
    $('.counter__done__num').html(result_done);
    $('.counter__not-done__num').html(result_not_done);
}


// Определение количества от


$(document).ready( function ()
{
    // Добавление новых задач через enter
    $('.page').keypress( function (event)
    {
        if (event.key === "Enter")
        {
            $('.ul').append("" +
                "<li class=\"fb fb_r-ct ul__li li" + counter + "\">\n" +
                "<h5 class='li__counter'>" + counter + "</h5>\n" +
                "<button class='li__button'>Не выполнил</button>\n" +
                "<input class='li__input-text' type=\"text\" value=''>\n" +
                "<div class=\"fb fb_c-sb li__div\">" +
                "<h5 class='li__div__text'></h5>" +
                "</div>\n" +
                "<button class=\"li__delete\">Удалить</button>\n" +
                "</li>");
            $('.li' + counter + ' .li__input-text').addClass("shadow-mask");
            $('.li' + counter + ' .li__button').addClass("li__input-text_not-done");
            $('.li' + counter + ' .li__div').addClass("light-mask");
            $('.li' + counter).addClass("light-mask_flex");
            counter++;
            recount();
        }
    });

    // Добавление новых задач
    $('.add-button').click( function ()
    {
        $('.ul').append("" +
        "<li class=\"fb fb_r-ct ul__li li" + counter + "\">\n" +
            "<h5 class='li__counter'>" + counter + "</h5>\n" +
            "<button class='li__button'>Не выполнил</button>\n" +
            "<input class='li__input-text' type=\"text\" value=''>\n" +
            "<div class=\"fb fb_c-sb li__div\">" +
                "<h5 class='li__div__text'></h5>" +
            "</div>\n" +
            "<button class=\"li__delete\">Удалить</button>\n" +
        "</li>");
        $('.li' + counter + ' .li__input-text').addClass("shadow-mask");
        $('.li' + counter + ' .li__button').addClass("li__input-text_not-done");
        $('.li' + counter + ' .li__div').addClass("light-mask");
        $('.li' + counter).addClass("light-mask_flex");
        counter++;
        recount();
    });

    // Редактирование задач
    $('.ul').on('dblclick', ".ul__li .li__div", function () {
        close_li();
        $(this).parent().children('.li__div').addClass("shadow-mask");
        $(this).parent().children('.li__div').removeClass("light-mask");
        $(this).parent().children('.li__input-text').removeClass("shadow-mask");
        $(this).parent().children('.li__input-text').addClass("light-mask");
        $(this).parent().children('.li__input-text').val($(this).parent().children('.li__div').text());
    });

    // Изменение статуса выполнения
    $('.ul').on('click', ".ul__li .li__button", function () {
        close_li();
        if ($(this).parent().children('.li__button').text() === "Не выполнил")
        {
            $(this).parent().children('.li__button').text("Выполнил");
            $(this).parent().children('.li__button').removeClass("li__input-text_not-done");
            $(this).parent().children('.li__button').addClass("li__input-text_done");
        }
        else
        {
            $(this).parent().children('.li__button').text("Не выполнил");
            $(this).parent().children('.li__button').addClass("li__input-text_not-done");
            $(this).parent().children('.li__button').removeClass("li__input-text_done");
        }
        recount();
    });

    // Удаление всех выполненных задач
    $('.delete').on('click', ".delete__done", function () {
        close_li();
        for (i = 0; i < counter; i++)
        {
            state = $('.ul .li' + (i + 1));
            if (state.length > 0)
            {
                if (state.children('.li__button').text() === "Выполнил")
                {
                    state.remove();
                }
            }
        }
        recount();
    });

    // Удаление отдельной задачи
    $('.ul').on('click', ".ul__li .li__delete", function () {
        close_li();
        $(this).parent().remove();
        recount();
    });

    // Отображение только выполненных
    $('.filter').on('click', ".filter__done", function () {
        close_li();
        for (i = 0; i < counter; i++)
        {
            state = $('.ul .li' + (i + 1));
            if (state.length > 0)
            {
                if (state.children('.li__button').text() !== "Выполнил")
                {
                    state.removeClass("light-mask_flex");
                    state.addClass("shadow-mask");
                }
                else
                {
                    state.addClass("light-mask_flex");
                    state.removeClass("shadow-mask");
                }
            }
        }
        filter_status = 1;
    });

    // Отображение только не выполненых
    $('.filter').on('click', ".filter__not-done", function () {
        close_li();
        for (i = 0; i < counter; i++)
        {
            state = $('.ul .li' + (i + 1));
            if (state.length > 0)
            {
                if (state.children('.li__button').text() !== "Не выполнил")
                {
                    state.removeClass("light-mask_flex");
                    state.addClass("shadow-mask");
                }
                else
                {
                    state.addClass("light-mask_flex");
                    state.removeClass("shadow-mask");
                }
            }
        }
        filter_status = 2;
    });

    // Отображение все задачи
    $('.filter').on('click', ".filter__all", function () {
        close_li();
        for (i = 0; i < counter; i++)
        {
            state = $('.ul .li' + (i + 1));
            if (state.length > 0)
            {
                state.addClass("light-mask_flex");
                state.removeClass("shadow-mask");
            }
        }
        filter_status = 0;
    });

    // Отметить все как выполненые
    $('.change').on('click', ".change__done", function () {
        close_li();
        for (i = 0; i < counter; i++)
        {
            state = $('.ul .li' + (i + 1));
            if (state.length > 0)
            {
                if (state.children('.li__button').text() === "Не выполнил")
                {
                    state.children('.li__button').text("Выполнил");
                    state.children('.li__button').removeClass("li__input-text_not-done");
                    state.children('.li__button').addClass("li__input-text_done");
                }
            }
        }
        recount();
    });

    // Отметить все как невыполненное
    $('.change').on('click', ".change__not-done", function () {
        close_li();
        for (i = 0; i < counter; i++)
        {
            state = $('.ul .li' + (i + 1));
            if (state.length > 0) {
                if (state.children('.li__button').text() === "Выполнил") {
                    state.children('.li__button').text("Не выполнил");
                    state.children('.li__button').addClass("li__input-text_not-done");
                    state.children('.li__button').removeClass("li__input-text_done");
                }
            }
        }
        recount();
    });
});



