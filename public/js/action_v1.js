// Model
let state_list = [];        //Список с данными задач
let page_size = 6;          //Количество отображаемых записей на странице
let current_page = 0;       //Текущая страницы
let view_status = 0;        //Включён ли фильтр выполненных
let i;                      //Удобности итератора



// Controller
$(document).ready( function () {

    //Инициация стартовых параметров и кэширование
    let adder_button = $(".adder__button");
    let adder_input = $(".adder__input");
    let filter_button_all =  $(".filter__button-all");
    let filter_button_done = $(".filter__button-done");
    let filter_button_not_done = $(".filter__button-not-done");
    let list__ul = $(".list__ul");
    let counter_text_done = $(".counter__text-done");
    let counter_text_not_done = $(".counter__text-not-done");
    let counter_text_all = $(".counter__text-all");
    let pager = $(".pager");
    let deleter_button = $(".deleter__button");
    let changer_button_not_done = $(".changer__button-not-done");
    let changer_button_done = $(".changer__button-done");
    start();



    // Событийные функции
    //Добавление новой задачи
    adder_button.click( function () {
        if (adder_input.val().trim() != '') {
            ajax_add_todo(false, _.escape(_.unescape(adder_input.val())).trim());
        }
    });

    //Добавление новой задачи по нажатию Enter
    $(".page").keypress( function (event) {
        if (event.key === "Enter") {
            if (adder_input.val().trim() != '') {
                if(focus_control())
                {
                    ajax_add_todo(false, _.escape(_.unescape(adder_input.val())).trim());
                }
            }
        }
    });

    //Изменение страницы
    pager.on("click", ".pager__button", function () {
        current_page = parseInt($(this).html()) - 1;
        update();
        return false;
    });

    //Фильтрация всех задач
    filter_button_all.click( function () {
        view_status = 0;
        focus_filter(view_status);
        for (i = 0; i < state_list.length; i++) {
            state_list[i][2] = true;
        }
        current_page = 0;
        update();
        return false;
    });

    //Фильтрация выполненных задач
    filter_button_done.click( function () {
        view_status = 1;
        focus_filter(view_status);
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i][0] === true) {
                state_list[i][2] = true;
            }
            else {
                state_list[i][2] = false;
            }
        }
        current_page = 0;
        update();
        return false;
    });

    //Фильтрация невыполненных задач
    filter_button_not_done.click( function () {
        view_status = 2;
        focus_filter(view_status);
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i][0] === false) {
                state_list[i][2] = true;
            }
            else {
                state_list[i][2] = false;
            }
        }
        current_page = 0;
        update();
        return false;
    });

    //Изменение состояния выполнения
    list__ul.on("click", ".li__button-change", function () {
        let hid = parseInt($(this).parent().children(".li__id").html());
        ajax_update_todo_private(state_list[hid][3], !state_list[hid][0]);
        return false;
    });

    //Удаление элемента
    list__ul.on("click", ".list__ul_li .li__button-delete", function () {
        let hid = parseInt($(this).parent().children(".li__id").html());
        ajax_delete_todo_private(state_list[hid][3]);
        return false;
    });

    //Удалить все выполненные
    deleter_button.click( function () {
        ajax_delete_todo_public();
        return false;
    });

    //Отметить все задачи как выполненные
    changer_button_done.click( function () {
        ajax_update_todo_public(true);
        return false;
    });

    //Отметить все задачи как невыполненные
    changer_button_not_done.click( function () {
        ajax_update_todo_public(false);
        return false;
    });


    //Редактирование текста задачи
    list__ul.on("keyup", ".list__ul_li .li__div-input input", function (event) {
        let hid = parseInt($(this).parent().parent().children('.li__id').html());
        if (event.key === "Enter") {
            ajax_update_todo_text(state_list[hid][3], _.escape(_.unescape($(this).val().trim())));
        }
        else {
            let answer_flag = false;
            if (_.escape(_.unescape($(this).val().trim())) === "") {
                answer_flag = confirm("Да или нет?");
                if (answer_flag) {
                    ajax_delete_todo_private(state_list[hid][3]);
                }
                else {
                    ajax_select_todo_text(state_list[hid][3], $(this));
                }
            }
        }
    });

    //Двойное нажатие для редактирования
    list__ul.on("dblclick", ".list__ul_li .li__div-output", function () {
        hide_inputs();
        let hid = parseInt($(this).parent().children(".li__id").html());
        let input = $(this).parent().children(".li__div-input").children("input");
        in_shadow($(this).parent().children(".li__div-output"));
        in_light($(this).parent().children(".li__div-input"));
        ajax_select_todo_text(state_list[hid][3], input);
        return false;
    });



    // Вспомогательные функции
    //Фокус фильтра
    function focus_filter(value) {
        filter_button_all.removeClass("filter__button_active");
        filter_button_done.removeClass("filter__button_active");
        filter_button_not_done.removeClass("filter__button_active");
        if (value === 0) { filter_button_all.addClass("filter__button_active"); }
        if (value === 1) { filter_button_done.addClass("filter__button_active"); }
        if (value === 2) { filter_button_not_done.addClass("filter__button_active"); }
    }

    //Стартовая функция
    function start() {
        ajax_select_todo_all();
    }

    //Проверка всех вводов на фокус
    function focus_control() {
        let result = true;
        for (i = 0; i < state_list.length; i++) {
            if ($(`.list__ul_li${i} .li__div-input input`).is(":focus")) {
                result = false;
            }
        }
        return result;
    }

    //Обработка полученных параметров записей после Ajax запросов
    function update_state_list(val) {
        state_list = [];
        for (i = 0; i < val.length; i++) {
            state_list.push([val[i]['done_flag'], val[i]['text'], true, val[i]['id']]);
        }
        state_list.sort((a, b) => {return a[3] - b[3]});
    }

    //Вывод значений листа для дебаггинга
    function print_list(val) {
        console.clear();
        for (i = 0; i < val.length; i++) {
            console.log(val[i]);
        }
    }

    //Скрытие всех вводов
    function hide_inputs() {
        for (i = 0; i < state_list.length; i++) {
            in_shadow($(`.list__ul_li${i} .li__div-input`));
            in_light($(`.list__ul_li${i} .li__div-output`));
        }
    }

    //Очищение массива с данными от удалённых
    function clear_model() {
        let state = [];
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i] != undefined) {
                state.push(state_list[i]);
            }
        }
        state_list = state;
    }

    //После обовления разпределяю видимость
    function refiltring() {
        if (view_status === 0) {
            for (i = 0; i < state_list.length; i++) {
                state_list[i][2] = true;
            }
        }
        if (view_status === 1) {
            for (i = 0; i < state_list.length; i++) {
                if (state_list[i][0] === true) {
                    state_list[i][2] = true;
                }
                else {
                    state_list[i][2] = false;
                }
            }
        }
        if (view_status === 2) {
            for (i = 0; i < state_list.length; i++) {
                if (state_list[i][0] === false) {
                    state_list[i][2] = true;
                }
                else {
                    state_list[i][2] = false;
                }
            }
        }
    }

    //Определение количества страниц
    function page_calculator() {
        let result = 0;
        let len = 0;
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i][2] === true) {
                len += 1;
            }
        }
        result += (len - len % page_size) / page_size;
        if (len % page_size > 0) { result++; }
        return result;
    }

    //Определение значения счётчика
    function get_count_value () {
        let done = 0;
        let not_done = 0;
        let all = 0;
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i][0]) { done ++; }
            else { not_done ++; }
            all ++;
        }
        return [done, not_done, all];
    }

    //Создание видимого списка
    function get_view_list() {
        let result = [];
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i][2] === true) {
                result.push(i);
            }
        }
        return result;
    }

    //Очищение списка от пустых сохранённых значений
    function delete_empty() {
        for (i = 0; i < state_list.length; i++) {
            if (state_list[i][1].trim() === '') {
                delete state_list[i];
            }
        }
        clear_model();
    }

    //Обновление и размещение содержимого
    function update() {
        clear();
        refiltring();
        let page_num = page_calculator();
        let view_list = get_view_list();
        let state = get_count_value();

        if (current_page > page_num-1) { current_page = page_num - 1; }
        if (page_num === 0) { current_page = 0; }
        for (i = 0; i < page_size; i++) {
            if (current_page * page_size + i < view_list.length) {
                view_todo(view_list[current_page * page_size + i]);
            }
        }
        clear_page();
        view_page(page_num);
        focusing_page();
        view_counter(state[0], state[1], state[2]);
        view_text();
    }



    // Viewer
    //Визуализация задачи по id
    function view_todo(value) {
        list__ul.append(`
            <li class='fb fb_r-sb list__ul_li list__ul_li${value}'>
               <h5 class='li__id'>${value}</h5>
               <button class='li__button-change'></button>
               <div class='li__div-input'><input type='text' maxlength='144'></div>
               <div class='li__div-output'><h5>${state_list[value][1]}</h5></div>
               <button class='li__button-delete'></button>
            </li>`);
        if (state_list[value][0]) {
            $(`.list__ul_li${value} .li__button-change`).html('Выполнил');
            $(`.list__ul_li${value} .li__button-change`).addClass("li__button-change_done");
            $(`.list__ul_li${value} .li__button-change`).removeClass("li__button-change_not-done");
        }
        else {
            $(`.list__ul_li${value} .li__button-change`).html('Не выполнил');
            $(`.list__ul_li${value} .li__button-change`).addClass("li__button-change_not-done");
            $(`.list__ul_li${value} .li__button-change`).removeClass("li__button-change_done");
        }
        in_shadow($(`.list__ul_li${value} .li__div-input`));
    }

    //Визуализация пагенации
    function view_page(value) {
        for (i = 0; i < value; i++) {
            pager.append(`<button class='pager__button pager__button${i}'>${i+1}</button>`);
        }
    }

    //Отображение даных из модели в тег текста задачи
    function view_text() {
        for (i = 0; i < state_list.length; i++) {
            $(`.list__ul_li${i}`).children(".li__div-output").children("h5").html(state_list[i][1]);
        }
    }

    //Визуализация счётчиков
    function view_counter(value1, value2, value3) {
        counter_text_done.html("Выполнено задач : " + value1);
        counter_text_not_done.html("Не выполнено задач : " + value2);
        counter_text_all.html("Всего задач : " + value3);
    }

    //Отрисовываю фокусировку выбранной страницы
    function focusing_page() {
        $(`.pager__button${current_page}`).addClass("pager__button_active");
    }

    //Дополнительная очистка страниц
    function clear_page() {
        pager.empty();
    }

    //Очищение поле ввода текста новых задач
    function clear_input() {
        adder_input.val("");
    }

    //Очистка рабочего пространства
    function clear() {
        pager.empty();
        list__ul.empty();
        counter_text_done.empty();
        counter_text_not_done.empty();
        counter_text_all.empty();
    }

    //Сокрытие блока
    function in_shadow(value) {
        value.addClass("shadow-mask").removeClass("light-mask");
    }

    //Раскрыть блок
    function in_light(value) {
        value.removeClass("shadow-mask").addClass("light-mask");
    }


    // Ajax функции
    //Ajax функция для добавления новой записи
    function ajax_add_todo(done_flag, text_value) {
        $.ajax({
            type: "POST",
            url: "/insert",
            data: JSON.stringify({done_flag: done_flag, text_value: text_value}),
            dataType: "json",
            contentType: "application/json",
            success: function(data){ update_state_list(data) }
        })
            .then( result_true => {
                if (page_calculator() != 0) { current_page = page_calculator() - 1 }
                clear_input();
                update();
            })
    }


    //Ajax функция для сохранения изменений в тексте одной записи
    function ajax_update_todo_text(id_todo, text) {
        $.ajax({
            type: "POST",
            url: "/update_text",
            data: JSON.stringify({id_todo: id_todo, text: text}),
            dataType: "json",
            contentType: "application/json",
            success: function(data){ update_state_list(data) }
        })
            .then( result_true => {
                update();
            })
    }

    //Ajax функция для изменения статуса записи по личной кнопке
    function ajax_update_todo_private(id_todo, done_flag) {
        $.ajax({
            type: "POST",
            url: "/update_private",
            data: JSON.stringify({id_todo: id_todo, done_flag: done_flag}),
            dataType: "json",
            contentType: "application/json",
            success: function(data){ update_state_list(data) }
        })
            .then( result_true => {
                update();
            })
    }

    //Ajax функция для удаления одной выбранной записи
    function ajax_delete_todo_private(id_todo) {
        $.ajax({
            type: "POST",
            url: "/delete_private",
            data: JSON.stringify({id_todo: id_todo}),
            dataType: "json",
            contentType: "application/json",
            success: function(data){ update_state_list(data) }
        })
            .then( result_true => {
                update();
            })
    }

    //Ajax функция для удаления всех выполненых записей
    function ajax_delete_todo_public() {
        $.ajax({
            type: "POST",
            url: "/delete_public",
            data: JSON.stringify({done_flag: true}),
            dataType: "json",
            contentType: "application/json",
            success: function(data){ update_state_list(data) }
        })
            .then( result_true => {
                update();
            })
    }

    //Ajax функция для изменения статуса записи по общей кнопки
    function ajax_update_todo_public(done_flag) {
        $.ajax({
            type: "POST",
            url: "/update_public",
            data: JSON.stringify({done_flag: done_flag}),
            dataType: "json",
            contentType: "application/json",
            success: function(data){ update_state_list(data) }
        })
            .then( result_true => {
                update();
            })
    }

    //Ajax функция для взятия значения текста
    function ajax_select_todo_text(id_todo, input) {
        $.ajax({
            type: "POST",
            url: "/select_text",
            data: JSON.stringify({id_todo: id_todo}),
            dataType: "json",
            contentType: "application/json",
            success: function(data){
                input.val(data[0]["text"]);
            }
        })
    }

    //Ajax функция выбоа всего
    function ajax_select_todo_all() {
        $.ajax({
            type: "POST",
            url: "/select_all",
            data: JSON.stringify({done_flag: true}),
            dataType: "json",
            contentType: "application/json",
            success: function(data){ update_state_list(data) }
        })
            .then(result_true => {
                focus_filter(0);
                update();
            })
    }
});